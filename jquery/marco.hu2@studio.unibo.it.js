$(document).ready(function(){
	var immagini = $("img");
	const size = immagini.length;
	var current = 0;
	$("div.slider-image img + img").hide();
	
	$("button#prev").click(function(e){
	$(immagini[current]).hide();
	current--;
	if(current<0) {
		current=size-1;
	}
	$(immagini[current]).fadeIn("slow");
    });
	
	$("button.foll").click(function(e){
	$(immagini[current]).hide();
	current++;
	if(current>size-1){
		current=0;
	}
	$(immagini[current]).fadeIn("slow");
    });

})
