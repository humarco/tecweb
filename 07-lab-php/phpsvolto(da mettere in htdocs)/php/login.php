<?php
require_once("bootstrap.php");

if(isset($_POST["username"]) && isset($_POST["password"])){
	$login_result = $dbh->checkLogin($_POST["username"], $_POST["password"]);
	if(count($login_result)==0){
		//login fallito
		$templateParams["errorelogin"]= "Errore!";
	}
	else{
		//login ok
		registerLoggedUser($login_result[0]);
	}
}
if(isUserLoggedIn()) {
	$templateParams["titolo"] = "Blog TW - ADMIN";
	$templateParams["nome"] = "login-home.php";
}
else {
	$templateParams["titolo"] = "Blog TW - Login";
	$templateParams["nome"] = "login-form.php";
}


$templateParams["articolicasuali"] = $dbh->getRandomPosts(2);
$templateParams["categorie"] = $dbh->getCategories();

$templateParams["articoli"] = $dbh->getPosts(2);


require("template/base.php");
?>