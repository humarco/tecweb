<!DOCTYPE html>
<?php 
	class DatabaseHelper{  //classe database
    private $db;

    public function __construct($servername, $username, $password, $dbname){
        $this->db = new mysqli($servername, $username, $password, $dbname);
        if($this->db->connect_error){
            die("Connesione fallita al db");
        }
    }

    public function check_var($var){
        $stmt = $this->db->prepare("SELECT id, valore, insieme FROM insiemi WHERE insieme = ?");
        $stmt->bind_param("i", $var);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

	
	public function insert($array) {
		$stmt = $this->db->prepare("SELECT MAX(insieme) as massimo FROM insiemi");
        $stmt->execute();
        $result = $stmt->get_result();
		$row = mysqli_fetch_array($result); 
		$max_insieme = $row['massimo'] + 1;
		foreach($array as $val) {
			$stmt = $this -> db->prepare("INSERT INTO insiemi(valore, insieme) VALUES (?,?)");
			$stmt->bind_param("ii", $val['valore'], $max_insieme);
			$stmt->execute();
		}
		
	}
}

	$dbh = new DatabaseHelper("localhost", "root", "", "giugno");
	if(isset($_GET['A']) && isset($_GET['B'])) {		//controlla  se le variabili sono state inserite
		$var_a = $_GET['A'];
		$var_b = $_GET['B'];
		if(is_numeric($var_a) && is_numeric($var_b) && $var_a>0 && $var_b >0) {			//controlla se le variabili sono numeriche e positive
			$array1 = $dbh->check_var($var_a);
			$array2 = $dbh->check_var($var_b);
			if(count($array1)!=0 && count($array2)!=0) {		//controlla se le variabili esistono nel DB
				$array1 = array_column($array1, "valore");
				$array2 = array_column($array2, "valore");
				$array3 = array_unique (array_merge ($array1, $array2));
				echo "Risultati unione:";
				foreach($array3 as $a){
					echo $a;
					echo " ";
				}
			}
			else {
				echo "A o B non è presente in DB.";
			}
		}
		else{
			echo "A o B non è un numero valido";
		}
	}
	else {
		echo "A o B non è settato.";
	}
?>
<html lang="it">
<head>
    <title>Esercizio preparazione php</title>
</head>
<body>
    <header>
        <h1>Esercizio preparazione php</h1>
    </header>
    <main>
       <form action="#" method="GET">
            <ul style="list-style-type:none">
                <li >
                    <label for="A">Var A:</label><input type="text" id="A" name="A"/>
                </li>
                <li>
                    <label for="B">Var B:</label><input type="text" id="B" name="B"/>
                </li>
                <li>
                    <input type="submit" name="submit" value="Invia" />
                </li>
            </ul>
        </form>
    </main>
</body>
</html>